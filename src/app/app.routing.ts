import { RouterModule, Routes } from '@angular/router'
import { LoginComponent } from './views/login/login.component'
import { RegisterComponent } from './views/register/register.component'
import { HomeComponent } from './views/home/home.component'
import { SmileComponent } from './views/smile/smile.component'

export const routes: Routes = [
  {
    path: 'register',
    component: RegisterComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'smile',
    component: SmileComponent
  },
  {
    path: '**',
    redirectTo: '/smile'
  }
];

export const routing = RouterModule.forRoot(routes);