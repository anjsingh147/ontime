export class VisualizationOptionModel {
    constructor() {
    }
    visualizationType: string;
    dataSourcesId: string;
    collectionName: string;
    query_obj: any;
    query_obj_array: any;
    limit:any;
    visualization_options: any
}