
import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Config } from '../config/general.config';

@Injectable()
export class NotAuthGuard implements CanActivate {
  constructor( private router: Router) {}
  // Function to determine whether user is authorized to view route
  canActivate() {
    // Check if user is logged in
    if (Config.getAuthTokenValid()) {
      this.router.navigate(['/']); // Return error, route to home
      return false; // Return false: user not allowed to view route
    } else {
      return true; // Return true: user is allowed to view route
    }
  }
}