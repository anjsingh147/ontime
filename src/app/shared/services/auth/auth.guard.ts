import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Config } from '../config/general.config';

@Injectable()
export class AuthGuard implements CanActivate {
  public redirectUrl;
  constructor( private router: Router) {}
  // Function to check if user is authorized to view route
  canActivate(
    router: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ) {
    // Check if user is logge din
    if (Config.getAuthTokenValid()) {
      return true; // Return true: User is allowed to view route
    } else {
      this.redirectUrl = state.url; // Grab previous url
      this.router.navigate(['/account/signin']);
      return false; // Return false: user not authorized to view page
    }
  }
}