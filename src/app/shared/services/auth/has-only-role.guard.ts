import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Config } from '../config/general.config';
import { USER_ROLES } from '../config/env.config';

import { AuthService } from './auth.service';

import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';

import { API_URL } from '../config/env.config';

@Injectable()
export class HasOnlyRoleGuard implements CanActivate {

  public role: string =  Config.getUserRole();
  constructor(private router: Router,
    private _objService: AuthService,
    private httpClient: HttpClient) {}
  // Function to check if user is authorized to view route
  canActivate(
    router: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
        // return true
    return new Promise((resolve, reject) => {
      this._objService.getUserDataByID(Config.getUserId()).subscribe(userData => {
            for (var i = 1; i <= router.data.role.length;) {
                if (USER_ROLES.indexOf(this.role) == USER_ROLES.indexOf(router.data.role[i-1])) {
                    resolve(true);
                    i++;
                    break;
                } else {
                    if (i == router.data.role.length) {
                      this.router.navigate([router.data.stateUrl]); // Return error and route to login page
                      resolve(false);
                      break;
                    }
                    i++;
                }
            }
        }, err => {})
    })
  }
}