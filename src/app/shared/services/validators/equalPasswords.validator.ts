import {FormControl, FormGroup} from "@angular/forms";

export class EqualPasswordsValidator {

  public static validate(firstField, secondField) {

    return (c:FormGroup) => {

      return (c.controls && c.controls[firstField].value == c.controls[secondField].value) ? null : {
        passwordsEqual: {
          valid: false
        }
      };
    }
  }



  public static passwordValidator(control: FormControl) {
    // {8,}           - Assert password is atleast 8  characters
    // (?=.*[0-9])       - Assert a string has at least one number
    var regex = /^(?=.*[0-9])[a-zA-Z0-9!@#$%^&*]{8,}$/;
    if (control.value && regex.test(control.value)) {
      return null;
    } else {
      return {'invalidPassword': true};
    }
  }


  public static matchingPasswords(passwordKey: string, confirmPasswordKey: string) {
    return (group: FormGroup): {[key: string]: any} => {
      let password = group.controls[passwordKey];
      let confirmPassword = group.controls[confirmPasswordKey];

      if (password.value !== confirmPassword.value) {
        return {
          mismatchedPasswords: true
        };
      }
    }
  }
}



