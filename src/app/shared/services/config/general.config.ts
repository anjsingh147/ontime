//import 'cloudinary';
//declare var cloudinary:any;
import { HOST_URL } from './env.config';
import { WEB_SOCKET_URL } from './env.config';
import { JwtHelper } from 'angular2-jwt';
import { tokenNotExpired } from 'angular2-jwt';
// import { Observable } from "rxjs/Observable";
import { Observable } from "rxjs/Rx";
import 'rxjs/add/observable/throw';

export const AUTH_TOKEN_KEY = "AuthToken";
export const ADMIN_ROUTE = "AdminRoute";
export const USERINFO = "UserInfo";
export const EXPIRES_ON = "DateTime";

export class Config {
    static jwtHelper: JwtHelper = new JwtHelper();

    static AuthToken = window.localStorage.getItem(AUTH_TOKEN_KEY);
    static AdminRoute = window.localStorage.getItem(ADMIN_ROUTE);
    static UserInfo = window.localStorage.getItem(USERINFO);
    static EXPIRES_ON = window.localStorage.getItem(EXPIRES_ON);

    static webSocketURL() {
        return WEB_SOCKET_URL;
    }

    // static clearToken():void {
    //     window.localStorage.clear();
    //     this.AuthToken = null;
    //     this.AdminRoute = null;
    //     this.UserInfo = null;
    // }

    static clearToken():void {
        window.localStorage.clear();
        this.AuthToken = null;
    }
    
    static getAuthTokenValid():boolean {
        return tokenNotExpired('AuthToken');
    }

    // static getAuthTokenValid():boolean {
    //     this.EXPIRES_ON = window.localStorage.getItem(EXPIRES_ON);

    //    var now:number        = new Date(new Date().getTime()).getTime(),
    //         expires_on: number = new Date(this.EXPIRES_ON).getTime(),
    //         diff:number       = expires_on - now ;

    //        diff = Math.floor(diff / 1e3);
    //     return  (diff/3600) > 1 ? true: false;
    // }

    // static setLoggedInToken(auth:string, userInfo:UserModel,expiryDate:string):void {
    //     window.localStorage.setItem(AUTH_TOKEN_KEY, auth);
    //     window.localStorage.setItem(USERINFO, JSON.stringify(userInfo));
    //     window.localStorage.setItem(EXPIRES_ON, expiryDate);
    // }

    static setLoggedInToken(auth:string):void {
        window.localStorage.setItem(AUTH_TOKEN_KEY, auth);
    }

    static getAuthToken():string {
        return this.AuthToken = window.localStorage.getItem(AUTH_TOKEN_KEY);
    }

    static getUserRole():string {
        return this.jwtHelper.decodeToken(localStorage.getItem(AUTH_TOKEN_KEY)).user_role;
    }

    static getUserId():string {
        return this.jwtHelper.decodeToken(localStorage.getItem(AUTH_TOKEN_KEY))._id;
    }

    static apiAccessToken() {
        var accessToken = { params: {access_token: this.getAuthToken()}};
        return accessToken;
    }

    static queryParamsWithPaginationQueryAndAPIAccessToken(queryString) {
        queryString["access_token"] = this.getAuthToken();
        return { params: queryString};
    }

    static handleError (error: any) {
        if (error.status == 401) { // Auth Error
            window.localStorage.clear();
            this.AuthToken = null;
            window.location.reload();
        }
        let errMsg = (error.message) ? error.message :
          error.status ? `${error.status} - ${error._body}` : "Server error";
        return Observable.throw(errMsg);    
    }

    static getUserInfoToken():string {
        return this.UserInfo = window.localStorage.getItem(USERINFO);
    }

    static setAdminRouteToken(auth:string):void {
        window.localStorage.setItem(ADMIN_ROUTE, auth);
    }

    static removeAdminRouteToken():void {
        window.localStorage.removeItem(ADMIN_ROUTE);
    }

    static getAdminRoute():string {
        return this.AdminRoute = window.localStorage.getItem(ADMIN_ROUTE);
    }

}
