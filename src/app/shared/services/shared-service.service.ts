import {Component, Injectable,Input,Output,EventEmitter, } from '@angular/core'
import { Subject } from 'rxjs/Subject';
import { BehaviorSubject } from 'rxjs';

// Name Service
export interface myData {
   name:string;
}

@Injectable()
export class SharedService {
  sharingData:any;
  isProfilePictureVisible: boolean;

  profilePictureChange: Subject<boolean> = new Subject<boolean>();
  MapDataChange: Subject<boolean> = new Subject<boolean>();

  private dataSource = new BehaviorSubject('default data');
  currentData = this.dataSource.asObservable();

  constructor()  {
        this.profilePictureChange.subscribe((value) => {
            this.isProfilePictureVisible = value
        });
    }

  saveData(Data){
    // console.log('save data function called' + str + this.sharingData.name);
    this.sharingData=Data; 
  };

  getData() {
    // console.log('get data function called');
    return this.sharingData;
  }

  changeProfilePicture() {
        this.profilePictureChange.next(!this.isProfilePictureVisible);
    }

  changeData(data: string) {
    this.dataSource.next(data)
  }
} 

    

    