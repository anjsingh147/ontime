/*
################################################################################
#  Pi-monk - An ai technology that works on innovation
#
################################################################################
#
#  Licensed Materials - Property of EBISU TECHNOLOGIES Pvt Ltd, 
#  (C) Copyright EBISU Pvt Ltd. 2018,  All rights reserved.
#  Use, duplication or disclosure restricted 
#  by Schedule Contract with Ebisu Pvt Ltd.
#
################################################################################
#
#  PROGRAM DESCRIPTION 
#
#   <Program Name>            |  <description>
#   Common Functions service   |  Common Functions Service associated properties  
#                             |  and methods, that can be included (via 
#                             |  dependency injection) into Angular 2  
#                             |  components. If allow you to develop code 
#                             |  for specific tasks that can be used in 
#                             |  those components. This service provides 
#                             |  http methods to integrates with backend to 
#                             |  query the Oganisations data.
#
#  =============================================================================
#
#  #############################################################################
#
#  REQUIREMENTS  - Injectable, Http, rxjs/Rx, rxjs/add/operator/map 
#
#  #############################################################################
#
#  Change Request - none
#
#  DATE            | Version           | CR#/SPRINT#/       |  PROGRAMMER         
#  ————————————————————————————————————————————————————————————————————————————
#   01-11-2018     |  Initial Version  | SPRINT v0.1.0      |  vikash chandra            
#                  |                   |                    |               
#                  |                   |                    |               
################################################################################ 
*/

import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/Rx';
import 'rxjs/add/operator/map';
import {Router} from '@angular/router';
import { Config } from './config/general.config';
import { API_URL } from './config/env.config';
import { Subscription } from 'rxjs/Rx';



@Injectable()
export class CommonFunctionsService {
private apiRoute:string = 'users';
   constructor(private _router:Router) {}
  
   requestTimeout(subs :Subscription,loader:any,loading:boolean,time:number){
    setTimeout(()=>{
        if(loading===true){
         subs.unsubscribe();
         alert('could not fetch the response from the given time interval');
         loading=false;
         loader.close();
         this._router.navigate(['/sessions/error']);
        }
  },time)
   }
}
