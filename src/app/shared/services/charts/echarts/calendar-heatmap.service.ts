import {
    Component,
    OnInit,
    OnDestroy,
    ChangeDetectorRef,
    Injectable
} from '@angular/core';

import {
    ChartDataService
} from '../chart-data.service';
import * as echarts from 'echarts'

@Injectable()
export class CalendarHeatmapService  {
  public heatMapData:any;
  constructor(private _objectService: ChartDataService) {}

month = ['jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul',
        'aug', 'sep', 'oct','nov','dec'];
days = ['Saturday', 'Friday', 'Thursday',
        'Wednesday', 'Tuesday', 'Monday', 'Sunday'];


//------------------------------------------------Methods------------------------------------------//

    makeChart(visualizationOptionData, chartData) {
        return this.createChartInstance(visualizationOptionData, chartData)
    }

    createChartInstance(visualizationOptionData, chartData) {
        let option = {
    // title: {
    //     top: 30,
    //     left: 'center',
    //     text: '2016'
    // },
    tooltip: {
        position: 'top',
        formatter: function (p) {
            var format = echarts.format.formatTime('yyyy-MM-dd', p.data[0]);
            return format + ': ' + p.data[1];
        }
    },
    animation: true,
    grid: {
        height: '100%',
        width: '100%',
        y: '0%',
        x: '0%'
    },
   
    // visualMap: {
    //     min: 0,
    //     max: 10,
    //     calculable: true,
    //     orient: 'horizontal',
    //     left: 'center',
    //     // bottom: '100',
    //     inRange : {   
    //         color: ['#007101', '#2a9402', '#74b900', '#e5f400', '#fbcd04', '#fda400', '#fd1500', '#f30100'] //From smaller to bigger value ->
    //     }
    // },

     visualMap: {
        min: 0,
        max: visualizationOptionData.maxValue,
        // type: 'piecewise',
        orient: 'horizontal',
        calculable: true,
        left: 'center',
        top: -10,
        inRange : {   
            // color: ['#007101', '#2a9402', '#74b900', '#e5f400', '#fbcd04', '#fda400', '#fd1500', '#f30100'] //From smaller to bigger value ->
            color: ['#cecece', '#fda400', '#007101', '#f30100'] //From smaller to bigger value ->
        }
    },


    // xAxis: {
    //     type: 'calendar',
    //     data: this.month,
    //     splitArea: {
    //         show: true
    //     }
    // },
    calendar: {
        // top: 30,
        // // left: -1,
        // right: 0,
        top: 50,
        bottom: 0,
        left: 60,
        right: 10,
        cellSize: ['auto', 20],
        range: visualizationOptionData.year,
        // itemStyle: {
        //     normal: {borderWidth: 0.1}
        // },
        yearLabel: {show: true}
    },
    series: {
        name: '',
        type: 'heatmap',
        coordinateSystem: 'calendar',
        calendarIndex: 0,
        data: chartData,
        label: {
            normal: {
                show: false
            }
        },
        itemStyle: {
            emphasis: {
                shadowBlur: 10,
                shadowColor: 'rgba(0, 0, 0, 0.5)'
            }
        }
    }
  };

    
    //     let  option = {
    //         title: {
    //             top: 30,
    //             left: 'center',
    //             text: visualizationOptionData.startDate+'  to '+ visualizationOptionData.endDate
    //         },
    //         tooltip : {},
    //         visualMap: {
    //             min: 0,
    //             max: visualizationOptionData.maxValue,
    //             type: 'piecewise',
    //             orient: 'horizontal',
    //             left: 'center',
    //             top: 65,
    //             textStyle: {
    //                 color: '#000'
    //             }
    //         },

    // //         yAxis: {
    // //     type: 'category',
    // //     data: this.days,
    // //     splitArea: {
    // //         show: true
    // //     }
    // // },
    // //         xAxis: {
    // //     type: 'calendar',
    // //     data: this.month,
    // //     splitArea: {
    // //         show: true
    // //     }

    // // },
    //         calendar: {
    //             top: 120,
    //             left: 30,
    //             right: 30,
    //             cellSize: ['auto', 13],
    //             range: visualizationOptionData.year,
    //             itemStyle: {
    //                 normal: {borderWidth: 0.5}
    //             },
    //             yearLabel: {show: false}
    //         },
    //         series: {
    //             type: 'heatmap',
    //             coordinateSystem: 'calendar',
    //             data: chartData
    //         }
    //     };
        
     return option;
    }
    
    
}