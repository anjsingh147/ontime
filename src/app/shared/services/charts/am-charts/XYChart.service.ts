import {
    Component,
    OnInit,
    OnDestroy,
    ChangeDetectorRef,
    Injectable
} from '@angular/core';

import {
    Router,
    ActivatedRoute,
    Params
} from '@angular/router';

import {
    ChartDataService
} from '../chart-data.service';

import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
// import am4themes_moonrisekingdom from "@amcharts/amcharts4/themes/dataviz";
// import am4themes_moonrisekingdom from "@amcharts/amcharts4/themes/material";
// import am4themes_moonrisekingdom from "@amcharts/amcharts4/themes/frozen";
// import am4themes_moonrisekingdom from "@amcharts/amcharts4/themes/kelly";

// import am4themes_animated from "@amcharts/amcharts4/themes/animated";

// // Themes begin
// // am4core.useTheme(am4themes_moonrisekingdom);
// am4core.useTheme(am4themes_animated);
// Themes end

@Injectable()
export class XYChartService implements OnInit {

    constructor(private _objectService: ChartDataService, private _router: Router) {}

    ngOnInit() {

    }


  am4themes_cs(target) {
  // if (target instanceof am4core.ColorSet) {
  //   target.list = [
  //     am4core.color("#2876BC"),
  //     am4core.color("#1FBCFF"),
  //     am4core.color("#00B800"),
  //     am4core.color("#83CE00"),
  //     am4core.color("#FEE83F"),
  //     am4core.color("#FFCF00"),
  //     am4core.color("#FFA500"),
  //     am4core.color("#FF7D00"),
  //     am4core.color("#EE5A30"),
  //     am4core.color("#D046B6"),                                                                     
  //     am4core.color("#653789"),
  //     am4core.color("#2A49A0")
  //   ];
  // }

// if (target instanceof am4core.ColorSet) {
//     target.list = [
//       am4core.color("#005f87"),
//       am4core.color("#828282"),
//       am4core.color("#24292e"),
//       am4core.color("#fbfbfb"),
//       am4core.color("#20a5ba"),
//       am4core.color("#c30771"),
//       am4core.color("#10a778"),
//       am4core.color("#008ec4"),
//       am4core.color("#ffbedc"),
//       am4core.color("#838383"),                                                                     
//       am4core.color("#1b1e23"),
//       am4core.color("#20bbfc"),
//       am4core.color("#6636b4"),
//       am4core.color("#d7d4f0"),

//     ];
//   }
  // --syntax_normal: #1b1e23;
  //   --syntax_comment: #828282;
  //   --syntax_diff: #24292e;
  //   --syntax_diff_bg: #fbfbfb;
  //   --syntax_number: #20a5ba;
  //   --syntax_keyword: #c30771;
  //   --syntax_atom: #10a778;
  //   --syntax_string: #008ec4;
  //   --syntax_error: #ffbedc;
  //   --syntax_unknown_variable: #838383;
  //   --syntax_known_variable: #005f87;
  //   --syntax_matchbracket: #20bbfc;
  //   --syntax_key: #6636b4;
  //   --selection: #d7d4f0;
  //   --hr: rgba(0,0,0,0.05);
  if (target instanceof am4core.Tooltip) {
    target.getFillFromObject = false;
    target.getStrokeFromObject = false;
    target.background.strokeOpacity = 0.4;
    target.background.fill = am4core.color("#000");
  }
  if (target instanceof am4core.Label) {
    target.fill = am4core.color("#555");
  }
  // if (target instanceof am4charts.Axis) {
  //   target.cursorTooltipEnabled = false;
  // }
  if (target instanceof am4charts.AxisRendererY) {
    target.grid.template.strokeOpacity = 0.1;
    // target.line.strokeOpacity = 1;
  }
  if (target instanceof am4charts.Grid) {
    target.strokeOpacity = 0.1;
    target.stroke = am4core.color("#000");
  }
  if (target instanceof am4core.InterfaceColorSet) {
    target.setFor("text", am4core.color("#333"));
    target.setFor("primaryButton", am4core.color("#666"));
    target.setFor("primaryButtonHover", am4core.color("#1FBCFF"));
    target.setFor("primaryButtonDown", am4core.color("#1FBCFF").lighten(-0.2));
    target.setFor(
      "primaryButtonActive",
      am4core.color("#1FBCFF").lighten(-0.2)
    );
    // target.setFor("primaryButtonText", am4core.color("#FFFFFF"));
    // target.setFor("primaryButtonStroke", am4core.color("#467B88"));
    // target.setFor("secondaryButton", am4core.color("#6DC0D5"));
    // target.setFor("secondaryButtonHover", am4core.color("#6DC0D5").lighten(-0.2));
    // target.setFor("secondaryButtonDown", am4core.color("#6DC0D5").lighten(-0.2));
    // target.setFor("secondaryButtonActive", am4core.color("#6DC0D5").lighten(-0.2));
    // target.setFor("secondaryButtonText", am4core.color("#FFFFFF"));
    // target.setFor("secondaryButtonStroke", am4core.color("#467B88"));
    // stroke
    // fill
    // disabledBackground
    // positive
    // negative
    // alternativeText
    // background
    // alternativeBackground
    // grid
    //target.setFor("grid", am4core.color("#ebebeb"));
  }
}

//------------------------------------------------Methods------------------------------------------//


    makeChart(visualizationData, visualizationOptionData, chartDiv, datasource_id, collectionDataName, query , limit) {
        let queryObj = {}
        if (query != undefined) {
            queryObj = query
        }
        this._objectService.getDataByCollectionNameWithDocumentFieldsNoPagination(datasource_id, collectionDataName, queryObj, limit)
        .subscribe(res => {
          this.createChartInstance(visualizationData, visualizationOptionData, chartDiv, res.dataList)
        }, err => {});
    }

    createChartInstance(visualizationData, visualizationOptionData, chartDiv, chartData) {
        am4core.useTheme(this.am4themes_cs);
        /* Create chart instance */
        var _self = this;
        var seriesField = visualizationOptionData.seriesField.seriesField
        var chart;
        if (visualizationOptionData.XYChart3D) {
            chart = am4core.create(chartDiv, am4charts.XYChart3D);
        } else {
            chart = am4core.create(chartDiv, am4charts.XYChart);
        }
         
        // Add data
        chart.data = chartData;
        // Create axes
        if (visualizationOptionData.selectedCategoryIsDate) {
          var categoryAxis = chart.xAxes.push(new am4charts.DateAxis());
          categoryAxis.renderer.grid.template.location = 0;
          categoryAxis.renderer.minGridDistance = 50;
          categoryAxis.renderer.grid.template.disabled = true;
          categoryAxis.renderer.fullWidthTooltip = true;
        } else {
          var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());

        categoryAxis.dataFields.category = visualizationOptionData.selectedCategory;
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.minGridDistance = 20;
        categoryAxis.renderer.cellStartLocation = 0.1;
        categoryAxis.renderer.cellEndLocation = 0.9;
        }

        let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.min = 0;
        valueAxis.max = visualizationOptionData.valueAxisMax;
        valueAxis.strictMinMax = visualizationOptionData.valueAxisStrictMinMax;
        valueAxis.calculateTotals = true;
        valueAxis.renderer.minWidth = 50;
        valueAxis.title.text = visualizationOptionData.valueAxisTitle;

        // Create series
        function createSeries(field, name, width) {

            // Set up series
            let series;
            if (visualizationOptionData.XYChart3D) {
              series = chart.series.push(new am4charts.ColumnSeries3D());
            } else {
              series = chart.series.push(new am4charts.ColumnSeries());
            }

            series.name = name;
            if (visualizationOptionData.valueYShowTotalPercent) {
                series.dataFields.valueYShow = "totalPercent";
                series.columns.template.tooltipText = "[bold]{name}[/]\n[font-size:14px]{categoryX}: {valueY.totalPercent.formatNumber('#.00')}%";

            } else {
                series.columns.template.tooltipText = "[bold]{name}[/]\n[font-size:14px]{categoryX}: {valueY}";
            }
            series.dataFields.valueY = field;
            if (visualizationOptionData.selectedCategoryIsDate) {
              series.dataFields.dateX = visualizationOptionData.selectedCategory;
            } else {
              series.dataFields.categoryX = visualizationOptionData.selectedCategory;
            }

            series.sequencedInterpolation = false;
            series.legendSettings.valueText = "{valueY}";
            series.columns.template.fillOpacity = .8;
            // let columnTemplate = series.columns.template;
            // columnTemplate.strokeWidth = 2;
            // columnTemplate.strokeOpacity = 1;
            // Make it stacked
            series.stacked = visualizationOptionData.stacked;
            series.clustered = visualizationOptionData.clustered;
            // Configure columns
            series.columns.template.width = am4core.percent(width);
            if (visualizationData != undefined) {
              if (visualizationData.visualization_leaf.visualization_id != undefined) {
                  series.columns.template.cursorOverStyle = am4core.MouseCursorStyle.pointer;
                  series.columns.template.events.on("hit", function(ev) {
                  let dataContext:any
                  dataContext = ev.target.dataItem.dataContext;
                  let dataFields:any
                  dataFields = ev.target.dataItem.component.dataFields;
                  let query_obj = visualizationData.visualization_leaf.query_obj;
                  query_obj[dataFields.categoryX] = dataContext[dataFields.categoryX];
                  // query_obj[dataFields.valueY] = dataContext[dataFields.valueY];
                   _self._router.navigate(['dashboard/leaf/' 
                             + visualizationData.visualization_id + '/' + _self.createStringUrl(query_obj)]);
                }, this);
              }
            }
            // Add label
            let labelBullet = series.bullets.push(new am4charts.LabelBullet());
            // labelBullet.label.text = "{valueY}";
            labelBullet.locationY = 0.5;

            series.columns.template.events.on("hit", function(ev) {
            }, this);

            return series;
        }

        for (var i = 0; i < seriesField.length; i++) {
          createSeries(seriesField[i].field, seriesField[i].name, seriesField[i].width);
        }
        // Legend
        // chart.legend = new am4charts.Legend();

        return chart;
    }

  createStringUrl(obj) {
    var stringUrl = ""; 
      for (var key in obj) { 
          if (stringUrl != "") { 
              stringUrl += "&"; 
          } 
          stringUrl += (key + "=" + encodeURIComponent(obj[key])); 
      } 
    return stringUrl
  }
}