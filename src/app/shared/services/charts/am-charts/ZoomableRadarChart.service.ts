
import {
    Component,
    OnInit,
    OnDestroy,
    ChangeDetectorRef,
    Injectable
} from '@angular/core';

import {
    ToastrService
} from 'ngx-toastr';

import {
    ChartDataService
} from '../chart-data.service';

import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
am4core.useTheme(am4themes_animated);

@Injectable()
export class ZoomableRadarChartService implements OnInit {

    constructor(private _objectService: ChartDataService, private toastrService: ToastrService) {}

    ngOnInit() {}

//------------------------------------------------Methods------------------------------------------//

    makeChart(visualizationOptionData, chartDiv, datasource_id, collectionDataName, query, limit) {
        let queryObj = {}
        if (query != undefined) {
            queryObj = query
        }
        this._objectService.getDataByCollectionNameWithDocumentFieldsNoPagination(datasource_id, collectionDataName, queryObj, limit)
            .subscribe(res => {
                if (res.dataList.length != 0) {
                    this.createChartInstance(visualizationOptionData, chartDiv, res.dataList)
                } else {
                    this.toastrService.error('NO Data Found!'); // Used to notify error responce
                }
            }, err => {
                this.toastrService.error(err); // Used to notify error responce
        });
    }

    createChartInstance(visualizationOptionData, chartDiv, chartData) {
        var _self = this;
        var seriesField = visualizationOptionData.seriesField.seriesField
        
        /* Create chart instance */
        let chart = am4core.create(chartDiv, am4charts.RadarChart);
        chart.hiddenState.properties.opacity = 0; // this creates initial fade-in
        chart.legend = new am4charts.Legend();

        chart.padding(20, 20, 20, 20);
        chart.data = chartData;
        let categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis() as any);
        categoryAxis.dataFields.category = visualizationOptionData.selectedCategory;
        categoryAxis.renderer.labels.template.location = 0.5;
        categoryAxis.renderer.tooltipLocation = 0.5;

        let valueAxis = chart.yAxes.push(new am4charts.ValueAxis() as any);
        valueAxis.tooltip.disabled = true;
        valueAxis.renderer.labels.template.horizontalCenter = "left";
        valueAxis.min = 0;

        function createSeries(field, name, width) {
            let series = chart.series.push(new am4charts.RadarColumnSeries());
            series.columns.template.width = am4core.percent(width);
            series.columns.template.tooltipText = "{name}: {valueY.value}";
            series.name = name;
            series.dataFields.categoryX = visualizationOptionData.selectedCategory;
            series.dataFields.valueY = field;
            series.stacked = true;
            series.columns.template.cursorOverStyle = am4core.MouseCursorStyle.pointer;
            series.columns.template.events.on("hit", function(ev) {
               // _self._router.navigate(['/n-2-analytics/script-table/zbsitif_5_tr_analysis_b-' + _self.n1id.split("-")[5]]);
            }, this);
        }

        for (var i = 0; i < seriesField.length; i++) {
          createSeries(seriesField[i].field, seriesField[i].name, seriesField[i].width);
        }

        chart.seriesContainer.zIndex = -1;

        chart.scrollbarX = new am4core.Scrollbar();
        chart.scrollbarX.exportable = false;
        chart.scrollbarY = new am4core.Scrollbar();
        chart.scrollbarY.exportable = false;

        chart.cursor = new am4charts.RadarCursor();
        chart.cursor.xAxis = categoryAxis;
        chart.cursor.fullWidthLineX = true;
        chart.cursor.lineX.strokeOpacity = 0;
        chart.cursor.lineX.fillOpacity = 0.1;
        chart.cursor.lineX.fill = am4core.color("#000000");
        return chart;
    }
}