import {
    Component,
    OnInit,
    OnDestroy,
    ChangeDetectorRef,
    Injectable
} from '@angular/core';

import {
    ToastrService
} from 'ngx-toastr';

import {
    ChartDataService
} from '../chart-data.service';

import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
am4core.useTheme(am4themes_animated);

@Injectable()
export class StackedAreaRadarService implements OnInit {

    constructor(private _objectService: ChartDataService, private toastrService: ToastrService) {}

    ngOnInit() {}

    //------------------------------------------------Methods------------------------------------------//

    makeChart(visualizationData, visualizationOptionData, chartDiv, datasource_id, collectionDataName, query, limit) {
        let queryObj = {}
        if (query != undefined) {
            queryObj = query
        }
        this._objectService.getDataByCollectionNameWithDocumentFieldsNoPagination(datasource_id, collectionDataName, queryObj, limit)
            .subscribe(res => {
                if (res.dataList.length != 0) {
                    this.createChartInstance(visualizationData, visualizationOptionData, chartDiv, res.dataList)
                } else {
                    this.toastrService.error('NO Data Found!'); // Used to notify error responce
                }
            }, err => {
                this.toastrService.error(err); // Used to notify error responce
            });
    }

    createChartInstance(visualizationData, visualizationOptionData, chartDiv, chartData) {
        var seriesField = visualizationOptionData.seriesField.seriesField

        var _self = this;
        let chart = am4core.create(chartDiv, am4charts.RadarChart);

        chart.data = chartData;


        chart.padding(0, 0, 0, 0);
        chart.radarContainer.dy = 50;
        chart.innerRadius = am4core.percent(50);
        chart.radius = am4core.percent(100);
        chart.zoomOutButton.padding(20, 20, 20, 20);
        chart.zoomOutButton.margin(20, 20, 20, 20);
        chart.zoomOutButton.background.cornerRadius(40, 40, 40, 40);
        chart.zoomOutButton.valign = "bottom";

        var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis() as any);
        categoryAxis.dataFields.category = visualizationOptionData.seriesCategory;
        categoryAxis.renderer.labels.template.location = 0.5;
        categoryAxis.mouseEnabled = false;

        var categoryAxisRenderer = categoryAxis.renderer;
        categoryAxisRenderer.cellStartLocation = 0;
        categoryAxisRenderer.tooltipLocation = 0.5;
        categoryAxisRenderer.grid.template.disabled = true;
        categoryAxisRenderer.ticks.template.disabled = true;

        categoryAxisRenderer.axisFills.template.fill = am4core.color("#e8e8e8");
        categoryAxisRenderer.axisFills.template.fillOpacity = 0.2;
        categoryAxisRenderer.axisFills.template.location = -0.5;
        categoryAxisRenderer.line.disabled = true;
        categoryAxisRenderer.tooltip.disabled = true;
        categoryAxis.renderer.labels.template.disabled = true;


        categoryAxis.adapter.add("maxZoomFactor", function(maxZoomFactor, target) {
            return target.dataItems.length / 5;
        })

        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis() as any);

        var valueAxisRenderer = valueAxis.renderer;

        valueAxisRenderer.line.disabled = true;
        valueAxisRenderer.grid.template.disabled = true;
        valueAxisRenderer.ticks.template.disabled = true;
        valueAxis.min = 0;
        valueAxis.renderer.tooltip.disabled = true;


        var series = chart.series.push(new am4charts.RadarSeries());
        series.name = visualizationOptionData.name;
        series.dataFields.categoryX = visualizationOptionData.seriesCategory;
        series.dataFields.valueY = visualizationOptionData.value;
        series.stacked = true;
        series.fillOpacity = 0.5;
        series.fill = chart.colors.getIndex(1);
        series.stacked = true;
        series.strokeOpacity = 0;
        series.dataItems.template.locations.categoryX = 0.5;
        series.sequencedInterpolation = true;
        series.sequencedInterpolationDelay = 50;
        series.tooltipText = "[bold]{categoryX}[/]\nTotal: {valueY.total}";
        series.tooltip.pointerOrientation = "vertical";
        series.tooltip.label.fill = am4core.color("#ffffff");
        series.tooltip.label.fontSize = "0.8em";
        series.tooltip.autoTextColor = false;



        function createSeries(seriesCategory, value, name) {
            var subSeries = chart.series.push(new am4charts.RadarSeries());
            subSeries.name = name;
            subSeries.dataFields.categoryX = seriesCategory;
            subSeries.dataFields.valueY = value;
            subSeries.stacked = true;
            subSeries.fillOpacity = 0.5;
            subSeries.fill = chart.colors.getIndex(0);
            subSeries.strokeOpacity = 0;
            subSeries.dataItems.template.locations.categoryX = 0.5;
            subSeries.sequencedInterpolation = true;
            subSeries.sequencedInterpolationDelay = 50;
        }

        for (var i = 0; i < seriesField.length; i++) {
            createSeries(seriesField[i].seriesCategory, seriesField[i].value, seriesField[i].name);
        }



        chart.seriesContainer.zIndex = -1;
        chart.scrollbarX = new am4core.Scrollbar();
        chart.scrollbarX.parent = chart.bottomAxesContainer;
        chart.scrollbarX.exportable = false;
        chart.scrollbarY = new am4core.Scrollbar();
        chart.scrollbarY.exportable = false;

        chart.padding(0, 0, 0, 0)

        chart.scrollbarY.padding(20, 0, 20, 0);
        chart.scrollbarX.padding(0, 20, 0, 80);

        chart.scrollbarY.background.padding(20, 0, 20, 0);
        chart.scrollbarX.background.padding(0, 20, 0, 80);


        chart.cursor = new am4charts.RadarCursor();
        chart.cursor.lineX.strokeOpacity = 1;
        chart.cursor.lineY.strokeOpacity = 0;
        chart.cursor.lineX.stroke = chart.colors.getIndex(1);
        chart.cursor.innerRadius = am4core.percent(30);
        chart.cursor.radius = am4core.percent(50);
        chart.cursor.selection.fill = chart.colors.getIndex(1);

        //-------------------------//

        let bullet = series.bullets.push(new am4charts.Bullet());
        bullet.fill = am4core.color("#000000");
        bullet.strokeOpacity = 0;
        // bullet.locationX = 0.5;

        let line = bullet.createChild(am4core.Line);
        line.x2 = -100;
        line.x1 = 0;
        line.y1 = 0;
        line.y1 = 0;
        line.strokeOpacity = 1;

        line.stroke = am4core.color("#000000");
        line.strokeDasharray = "2,3";
        line.strokeOpacity = 0.4;


        let bulletValueLabel = bullet.createChild(am4core.Label);
        bulletValueLabel.text = "{valueY.total}";
        bulletValueLabel.verticalCenter = "middle";
        bulletValueLabel.horizontalCenter = "right";
        bulletValueLabel.dy = -3;

        let label = bullet.createChild(am4core.Label);
        label.text = "{categoryX}";
        label.verticalCenter = "middle";
        label.paddingLeft = 20;

        valueAxis.calculateTotals = true;


        chart.legend = new am4charts.Legend();
        chart.legend.parent = chart.radarContainer;
        chart.legend.width = 110;
        chart.legend.horizontalCenter = "middle";
        chart.legend.markers.template.width = 22;
        chart.legend.markers.template.height = 18;
        chart.legend.markers.template.dy = 2;
        chart.legend.labels.template.fontSize = "0.7em";
        chart.legend.dy = 20;
        chart.legend.dx = -9;

        chart.legend.itemContainers.template.cursorOverStyle = am4core.MouseCursorStyle.pointer;
        let itemHoverState = chart.legend.itemContainers.template.states.create("hover");
        itemHoverState.properties.dx = 5;

        let title = chart.radarContainer.createChild(am4core.Label);
        // title.text = "COMPANIES WITH\nTHE MOST CASH\nHELD OVERSEAS"
        title.fontSize = "1.2em";
        title.verticalCenter = "bottom";
        title.textAlign = "middle";
        title.horizontalCenter = "middle";
        title.fontWeight = "800";

        chart.maskBullets = false;

        let circle = bullet.createChild(am4core.Circle);
        circle.radius = 2;
        let hoverState = circle.states.create("hover");

        hoverState.properties.scale = 5;

        bullet.events.on("positionchanged", function(event) {
            event.target.children.getIndex(0).invalidate();
            event.target.children.getIndex(1).invalidatePosition();
        })


        bullet.adapter.add("dx", function(dx, target) {
            let angle = categoryAxis.getAngle(target.dataItem, "categoryX", 0.5);
            return 20 * am4core.math.cos(angle);
        })

        bullet.adapter.add("dy", function(dy, target) {
            let angle = categoryAxis.getAngle(target.dataItem, "categoryX", 0.5);
            return 20 * am4core.math.sin(angle);
        })

        bullet.adapter.add("rotation", function(dy, target) {
            let angle = Math.min(chart.endAngle, Math.max(chart.startAngle, categoryAxis.getAngle(target.dataItem, "categoryX", 0.5)));
            return angle;
        })


        line.adapter.add("x2", function(x2, target) {
            let dataItem = target.dataItem;
            if (dataItem) {
                let position = valueAxis.valueToPosition(dataItem.values.valueY.value + dataItem.values.valueY.stack);
                return -(position * valueAxis.axisFullLength + 35);
            }
            return 0;
        })


        bulletValueLabel.adapter.add("dx", function(dx, target) {
            let dataItem = target.dataItem;

            if (dataItem) {
                let position = valueAxis.valueToPosition(dataItem.values.valueY.value + dataItem.values.valueY.stack);
                return -(position * valueAxis.axisFullLength + 40);
            }
            return 0;
        })


        chart.seriesContainer.zIndex = 10;
        categoryAxis.zIndex = 11;
        valueAxis.zIndex = 12;

        chart.radarContainer.zIndex = 20;


        let previousBullets = [];
        // series.events.on("tooltipshownat", function(event) {
        //     let dataItem = event.dataItem;

        //     for (let i = 0; i < previousBullets.length; i++) {
        //         previousBullets[i].isHover = false;
        //     }

        //     previousBullets = [];

        //     let itemBullet = dataItem.bullets.getKey(bullet.uid);

        //     // for (let i = 0; i < itemBullet.children.length; i++) {
        //     //     let sprite = itemBullet.children.getIndex(i);
        //     //     sprite.isHover = true;
        //     //     previousBullets.push(sprite);
        //     // }
        // })

        // series.tooltip.events.on("visibilitychanged", function() {
        //     if (!series.tooltip.visible) {
        //         for (let i = 0; i < previousBullets.length; i++) {
        //             previousBullets[i].isHover = false;
        //         }
        //     }
        // })

        chart.cursorOverStyle = am4core.MouseCursorStyle.pointer;
        chart.events.on("hit", function(ev) {
            // _self._router.navigate(['/n-2-analytics/script-table/zbsitif_7']);
        }, this);

        chart.events.on("maxsizechanged", function() {
            if (chart.pixelInnerRadius < 200) {
                title.disabled = true;
                chart.legend.verticalCenter = "middle";
                chart.legend.dy = 0;
            } else {
                title.disabled = false;
                chart.legend.verticalCenter = "top";
                chart.legend.dy = 20;
            }
        })
        return chart;
    }
}