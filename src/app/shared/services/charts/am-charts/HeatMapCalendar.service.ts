import {
    Component,
    OnInit,
    OnDestroy,
    ChangeDetectorRef,
    Injectable
} from '@angular/core';

import {
    ChartDataService
} from '../chart-data.service';

import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";


import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import * as echarts from 'echarts'
am4core.useTheme(am4themes_animated);

@Injectable()
export class HeatMapCalendarService  {
  public heatMapData:any
  constructor(private _objectService: ChartDataService) {}

 month = ['jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul',
        'aug', 'sep', 'oct','nov','dec'];
days = ['Saturday', 'Friday', 'Thursday',
        'Wednesday', 'Tuesday', 'Monday', 'Sunday'];

    public resList=[
        ["2019-01-12", 5252] ,
        ["2019-01-16", 2117],
        ["2019-01-01", 7623],
        ["2019-01-02", 4197] ,
        ["2019-01-03", 6945] ,
        ["2019-01-04", 2124] ,
        ["2019-01-05", 9185],
        ["2019-01-10", 4527],
        ["2019-01-15", 1386],
        ["2019-03-02", 233] ,
        ["2019-03-01", 8885] ,
        ["2019-02-26", 5339],
        ["2019-02-25", 9138] ,
        ["2019-02-25", 9138] ,
        ["2019-02-23", 1235]
   ]      

getVirtulData(year) {
// year = year || '2019';
// // var date = +echarts.number.parseDate(year + '-01-01');
// // var end = +echarts.number.parseDate((+year + 1) + '-01-01');
// var date = +echarts.number.parseDate(this.heatMapData.startDate);
// var end = +echarts.number.parseDate(this.heatMapData.endDate);
// var dayTime = 3600 * 24 * 1000;
// // var data = [];
// // for (var time = date; time < end; time += dayTime) {
// //     data.push([
// //         echarts.format.formatTime('yyyy-MM-dd', time),
// //         Math.floor(Math.random() * this.heatMapData.maxValue)
// //     ]);
// //}
var data=this.resList
console.log(data)
return data;
}


//------------------------------------------------Methods------------------------------------------//


    makeChart(visualizationOptionData,selectedDataSource, chartDiv, collectionDataName, query , limit) {
        console.log('visualization option data')
        console.log(visualizationOptionData)
        console.log('in the heatmapcalendar service')
        this.heatMapData=visualizationOptionData
        // this._objectService.getDataByCollectionNameWithDocumentFieldsNoPagination(collectionDataName)
        //     .subscribe(res => {
        //       this.createChartInstance(visualizationOptionData, chartDiv, res.dataList)
        //     }, err => {});
       return  this.createChartInstance()
    }

    createChartInstance() {
    
        let  option = {
            title: {
                top: 30,
                left: 'center',
                text: this.heatMapData.startDate+'  to '+this.heatMapData.endDate
            },
            tooltip : {},
            visualMap: {
                min: 0,
                max: this.heatMapData.maxValue,
                type: 'piecewise',
                orient: 'horizontal',
                left: 'center',
                top: 65,
                textStyle: {
                    color: '#000'
                }
            },

    //         yAxis: {
    //     type: 'category',
    //     data: this.days,
    //     splitArea: {
    //         show: true
    //     }
    // },
    //         xAxis: {
    //     type: 'calendar',
    //     data: this.month,
    //     splitArea: {
    //         show: true
    //     }

    // },
            calendar: {
                top: 120,
                left: 30,
                right: 30,
                cellSize: ['auto', 13],
                range: this.heatMapData.year,
                itemStyle: {
                    normal: {borderWidth: 0.5}
                },
                yearLabel: {show: false}
            },
            series: {
                type: 'heatmap',
                coordinateSystem: 'calendar',
                data: this.getVirtulData(this.heatMapData.year)
            }
        };
        
     return option;
    }
    
    
}