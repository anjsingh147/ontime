import {
    Component,
    OnInit,
    OnDestroy,
    ChangeDetectorRef,
    Injectable
} from '@angular/core';

import {
    ChartDataService
} from '../chart-data.service';

import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
am4core.useTheme(am4themes_animated);

@Injectable()
export class TreeMapChartService  {

    constructor(private _objectService: ChartDataService) {}



//------------------------------------------------Methods------------------------------------------//


    makeChart(visualizationOptionData, chartDiv, data_source_id, collectionDataName, query , limit) {
    //     this._objectService.getDataByCollectionNameWithDocumentFieldsNoPagination(data_source_id, collectionDataName, query)
    //         .subscribe(res => {
    //           this.createChartInstance(visualizationOptionData, chartDiv, res.dataList)
    //         }, err => {});
    }

    createChartInstance(visualizationOptionData, chartDiv, chartData) {
        // Create chart instance
        let chart= am4core.create(chartDiv,am4charts.TreeMap)
        chart.data=chartData
        chart.dataFields.value=visualizationOptionData.selectedValue;
        chart.dataFields.name=visualizationOptionData.selectedCategory;
        chart.legend= new am4charts.Legend();
        chart.data = chartData;
        return chart;


        // var chart = am4core.create(chartDiv, am4charts.PieChart);

        // // Add and configure Series
        // var pieSeries = chart.series.push(new am4charts.PieSeries());
        // pieSeries.dataFields.value = visualizationOptionData.selectedValue;
        // pieSeries.dataFields.category = visualizationOptionData.selectedCategory;

        // chart.innerRadius = am4core.percent(visualizationOptionData.innerRadius);

        // // Put a thick white border around each Slice
        // pieSeries.slices.template.stroke = am4core.color("#fff");
        // pieSeries.slices.template.strokeWidth = 2;
        // pieSeries.slices.template.strokeOpacity = 1;
        // pieSeries.slices.template
        //     // change the cursor on hover to make it apparent the object can be interacted with
        //     .cursorOverStyle = [{
        //         "property": "cursor",
        //         "value": "pointer"
        //     }];

        // pieSeries.alignLabels = false;
        // const myColors= [
        //     "#fe6989",
        //     "#fd9428",
        //     "#fecf5b",
        //     "#50c4c3",
        //     "#FFC75F",
        //     "#F9F871"
        //   ];

        //   // pieSeries.slices.template.adapter.add("fill", (fill, target) => {
        //   //   return am4core.color(myColors[target.dataItem.index]);
        //   // });

        // pieSeries.hiddenState.properties.endAngle = -90;
        // if (visualizationOptionData.radiusValue) {
        //     pieSeries.dataFields.radiusValue = visualizationOptionData.selectedValue;
        // }
        // pieSeries.slices.template.cornerRadius = visualizationOptionData.cornerRadius;

        // pieSeries.ticks.template.disabled = true;
        // // pieSeries.labels.template.text = "{value.percent.formatNumber('#.0')}%";
        // // pieSeries.labels.template.radius = am4core.percent(-40);
        // // pieSeries.labels.template.fill = am4core.color("white");
        // // pieSeries.labels.template.relativeRotation = 90;
        // pieSeries.labels.template.disabled = true;
        // // Create a base filter effect (as if it's not there) for the hover to return to
        // var shadow = pieSeries.slices.template.filters.push(new am4core.DropShadowFilter);
        // shadow.opacity = 0;

        // // Create hover state
        // var hoverState = pieSeries.slices.template.states.getKey("hover"); // normally we have to create the hover state, in this case it already exists

        // // Slightly shift the shadow and make it more prominent on hover
        // var hoverShadow = hoverState.filters.push(new am4core.DropShadowFilter);
        // hoverShadow.opacity = 0.7;
        // hoverShadow.blur = 5;

        // // Add a legend
        // chart.legend = new am4charts.Legend();
        // chart.legend.position = "right";
        // // chart.legend.marginLeft = 0;
        // // chart.legend.marginRight = 0;
        // // chart.legend.labels.template.maxWidth = 50;
        // chart.legend.width = 200;
        // chart.legend.labels.template.truncate = false;
        // let markerTemplate = chart.legend.markers.template;
        //     markerTemplate.width = 15;
        //     markerTemplate.height = 15;

        // pieSeries.slices.template.cursorOverStyle = am4core.MouseCursorStyle.pointer;
        // pieSeries.slices.template.events.on("hit", function(ev) {
        // }, this);

        // chart.data = chartData;
       // return chart;
    }
}