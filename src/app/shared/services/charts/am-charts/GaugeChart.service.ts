import {
    Component,
    OnInit,
    OnDestroy,
    ChangeDetectorRef,
    Injectable
} from '@angular/core';

import {
    ChartDataService
} from '../chart-data.service';

import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
am4core.useTheme(am4themes_animated);

@Injectable()
export class GaugeChartService implements OnInit {

    constructor(private _objectService: ChartDataService) {}

    ngOnInit() {}

//------------------------------------------------Methods------------------------------------------//


    makeChart(visualizationOptionData, chartDiv, datasource_id, collectionDataName, query , limit) {
        let queryObj = {}
        if (query != undefined) {
            queryObj = query
        }
        this._objectService.getDataByCollectionNameWithDocumentFieldsNoPagination(datasource_id, collectionDataName, queryObj, limit)
            .subscribe(res => {
              this.createChartInstance(visualizationOptionData, chartDiv, res.dataList)
            }, err => {});
    }

    createChartInstance(visualizationOptionData, chartDiv, chartData) {
        let data = chartData.find(item => item[visualizationOptionData.selectedCategory] === visualizationOptionData.categorieValues);
        // Create chart instance
        var chart = am4core.create(chartDiv, am4charts.GaugeChart);
        chart.innerRadius = am4core.percent(visualizationOptionData.innerRadius);
        // chart.colors.list = [
        //     am4core.color("#6771dc")
        // ];

        /**
         * Normal axis
         */

        let axis = chart.xAxes.push(new am4charts.ValueAxis() as any);
        axis.min = visualizationOptionData.minRang;
        axis.max = visualizationOptionData.maxRang;
        axis.strictMinMax = true;
        axis.renderer.radius = am4core.percent(visualizationOptionData.radius);
        axis.renderer.inside = true;
        axis.renderer.line.strokeOpacity = 1;
        axis.renderer.ticks.template.disabled = false
        axis.renderer.ticks.template.strokeOpacity = 1;
        axis.renderer.ticks.template.length = 10;
        axis.renderer.grid.template.disabled = true;
        axis.renderer.labels.template.radius = 40;
        axis.renderer.labels.template.adapter.add("text", function(text) {
            return text;
        })

        /**
         * Axis for ranges
         */

        let colorSet = new am4core.ColorSet();

        let axis2 = chart.xAxes.push(new am4charts.ValueAxis() as any);
        axis2.min = visualizationOptionData.minRang;
        axis2.max = visualizationOptionData.maxRang;
        axis2.renderer.innerRadius = 10
        // axis2.strictMinMax = true;
        axis2.renderer.labels.template.disabled = true;
        axis2.renderer.ticks.template.disabled = true;
        axis2.renderer.grid.template.disabled = true;

        let range0 = axis2.axisRanges.create();
        range0.value = 0;
        range0.endValue = 50;
        range0.axisFill.fillOpacity = 1;
        range0.axisFill.fill = colorSet.getIndex(0);

        let range1 = axis2.axisRanges.create();
        range1.value = 50;
        range1.endValue = 0;
        range1.axisFill.fillOpacity = 1;
        range1.axisFill.fill = colorSet.getIndex(2);

        /**
         * Label
         */

        let label = chart.radarContainer.createChild(am4core.Label);
        label.isMeasured = false;
        label.fontSize = 45;
        label.x = am4core.percent(50);
        label.y = am4core.percent(100);
        label.horizontalCenter = "middle";
        label.verticalCenter = "bottom";
        label.text = data[visualizationOptionData.selectedHandValueCategorie];

        let legend = new am4charts.Legend();
        legend.isMeasured = true;
        legend.y = am4core.percent(100);
        legend.verticalCenter = "bottom";
        legend.parent = chart.chartContainer;
        legend.data = [{
            "name": visualizationOptionData.legendName,
            "fill": chart.colors.getIndex(0)
        }];

        let _self = this;
        legend.itemContainers.template.events.on("hit", function(ev) {
        });

        /**
         * Hand
         */

        let hand = chart.hands.push(new am4charts.ClockHand());
        hand.axis = axis2;
        hand.innerRadius = am4core.percent(20);
        hand.startWidth = 10;
        hand.pin.disabled = true;
        hand.value = data[visualizationOptionData.selectedHandValueCategorie];

        hand.events.on("propertychanged", function(ev) {
            range0.endValue = ev.target.value;
            range1.value = ev.target.value;
            axis2.invalidate();
        });
        return chart;
    }
}