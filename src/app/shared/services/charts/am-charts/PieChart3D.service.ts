import {
    Component,
    OnInit,
    OnDestroy,
    ChangeDetectorRef,
    Injectable
} from '@angular/core';

import {
    ChartDataService
} from '../chart-data.service';

import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
am4core.useTheme(am4themes_animated);

@Injectable()
export class PieChart3DService implements OnInit {

    constructor(private _objectService: ChartDataService) {}

    ngOnInit() {}

    //------------------------------------------------Methods------------------------------------------//


    makeChart(visualizationOptionData, chartDiv, datasource_id, collectionDataName, query , limit) {
        let queryObj = {}
        if (query != undefined) {
            queryObj = query
        }
        // this._objectService.getDataByCollectionNameWithDocumentFieldsNoPagination(datasource_id, collectionDataName, queryObj)
        //     .subscribe(res => {
        //         this.createChartInstance(visualizationOptionData, chartDiv, res.dataList)
        //     }, err => {});
    }

    createChartInstance(visualizationOptionData, chartDiv, chartData) {
        // // Create chart instance
        // var chart = am4core.create(chartDiv, am4charts.PieChart);

        // // Add and configure Series
        // var pieSeries = chart.series.push(new am4charts.PieSeries());
        // pieSeries.dataFields.value = visualizationOptionData.selectedValue;
        // pieSeries.dataFields.category = visualizationOptionData.selectedCategory;

        // chart.innerRadius = am4core.percent(visualizationOptionData.innerRadius);

        // // Put a thick white border around each Slice
        // pieSeries.slices.template.stroke = am4core.color("#fff");
        // pieSeries.slices.template.strokeWidth = 2;
        // pieSeries.slices.template.strokeOpacity = 1;
        // pieSeries.slices.template
        //     // change the cursor on hover to make it apparent the object can be interacted with
        //     .cursorOverStyle = [{
        //         "property": "cursor",
        //         "value": "pointer"
        //     }];

        // pieSeries.alignLabels = false;
        // const myColors = [
        //     "#fe6989",
        //     "#fd9428",
        //     "#fecf5b",
        //     "#50c4c3",
        //     "#FFC75F",
        //     "#F9F871"
        // ];

        // // pieSeries.slices.template.adapter.add("fill", (fill, target) => {
        // //   return am4core.color(myColors[target.dataItem.index]);
        // // });

        // pieSeries.hiddenState.properties.endAngle = -90;
        // if (visualizationOptionData.radiusValue) {
        //     pieSeries.dataFields.radiusValue = visualizationOptionData.selectedValue;
        // }
        // pieSeries.slices.template.cornerRadius = visualizationOptionData.cornerRadius;

        // pieSeries.ticks.template.disabled = true;
        // // pieSeries.labels.template.text = "{value.percent.formatNumber('#.0')}%";
        // // pieSeries.labels.template.radius = am4core.percent(-40);
        // // pieSeries.labels.template.fill = am4core.color("white");
        // // pieSeries.labels.template.relativeRotation = 90;
        // pieSeries.labels.template.disabled = true;
        // // Create a base filter effect (as if it's not there) for the hover to return to
        // var shadow = pieSeries.slices.template.filters.push(new am4core.DropShadowFilter);
        // shadow.opacity = 0;

        // // Create hover state
        // var hoverState = pieSeries.slices.template.states.getKey("hover"); // normally we have to create the hover state, in this case it already exists

        // // Slightly shift the shadow and make it more prominent on hover
        // var hoverShadow = hoverState.filters.push(new am4core.DropShadowFilter);
        // hoverShadow.opacity = 0.7;
        // hoverShadow.blur = 5;

        // // Add a legend
        // chart.legend = new am4charts.Legend();
        // chart.legend.position = "right";
        // // chart.legend.marginLeft = 0;
        // // chart.legend.marginRight = 0;
        // // chart.legend.labels.template.maxWidth = 50;
        // chart.legend.width = 200;
        // chart.legend.labels.template.truncate = false;
        // let markerTemplate = chart.legend.markers.template;
        // markerTemplate.width = 15;
        // markerTemplate.height = 15;

        // pieSeries.slices.template.cursorOverStyle = am4core.MouseCursorStyle.pointer;
        // pieSeries.slices.template.events.on("hit", function(ev) {}, this);

        // chart.data = chartData;
        // return chart;


        //----------//
         var chart = am4core.create("UserLogindayDifferenceDiv", am4charts.PieChart3D);
         
            chart.hiddenState.properties.opacity = 0; // this creates initial fade-in
            chart.responsive.enabled = true;
            chart.colors.list = [
                am4core.color("#fe6989"),
                am4core.color("#fd9428"),
                am4core.color("#fecf5b"),
                am4core.color("#50c4c3"),
                am4core.color("#FFC75F"),
                am4core.color("#F9F871")
            ];
            chart.data = chartData;

            chart.innerRadius = am4core.percent(visualizationOptionData.innerRadius);
            chart.depth = 120;

            chart.legend = new am4charts.Legend();
            chart.legend.position = "right";
            chart.legend.marginLeft = 0;
            chart.legend.marginRight = 0;
            // chart.legend.labels.template.maxWidth = 50;
            chart.legend.width = 150;
            chart.legend.labels.template.truncate = false;
            let markerTemplate = chart.legend.markers.template;
                markerTemplate.width = 15;
                markerTemplate.height = 15;

            var series = chart.series.push(new am4charts.PieSeries3D());
            series.dataFields.value = visualizationOptionData.selectedValue;
            series.dataFields.depthValue = visualizationOptionData.selectedValue;
            series.dataFields.category = visualizationOptionData.selectedCategory;
            series.dataFields.depthValue = visualizationOptionData.selectedValue;
            series.labels.template.disabled = true;
            series.slices.template.cornerRadius = visualizationOptionData.cornerRadius;
            if (visualizationOptionData.radiusValue) {
                series.dataFields.radiusValue = visualizationOptionData.selectedValue;
            }
            const myColors= [
                "#fe6989",
                "#fd9428",
                "#fecf5b",
                "#50c4c3",
                "#FFC75F",
                "#F9F871"
              ];

              // series.slices.template.adapter.add("fill", (fill, target) => {
              //   return am4core.color(myColors[target.dataItem.index]);
              // });
            // series.colors.step = 3;
            series.labels.template.text = "{category}: {value.value} days";
            series.slices.template.tooltipText = "{category}: {value.value} days";
            chart.legend.valueLabels.template.text = "{value.value} days";

            series.slices.template.cursorOverStyle = am4core.MouseCursorStyle.pointer;
            series.slices.template.events.on("hit", function(ev) {
            }, this);
            
            return chart;
    }
}