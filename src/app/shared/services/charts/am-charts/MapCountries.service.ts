import {
    Component,
    OnInit,
    OnDestroy,
    ChangeDetectorRef,
    Injectable
} from '@angular/core';

import {
    ChartDataService
} from '../chart-data.service';

import { SharedService } from '../../shared-service.service';


import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import * as am4maps from "@amcharts/amcharts4/maps";
import am4geodata_worldLow from "@amcharts/amcharts4-geodata/worldLow";
import am4geodata_continentsLow from "@amcharts/amcharts4-geodata/continentsLow";

import am4themes_animated from "@amcharts/amcharts4/themes/animated";
am4core.useTheme(am4themes_animated);

@Injectable()
export class MapCountriesService implements OnInit {

    constructor(private _objectService: ChartDataService, private _sharedService: SharedService) {}

    ngOnInit() {}

//------------------------------------------------Methods------------------------------------------//


    makeChart(visualizationOptionData, chartDiv, datasource_id, collectionDataName, query , limit) {
        let queryObj = {}
        if (query != undefined) {
            queryObj = query
        }
        this._objectService.getDataByCollectionNameWithDocumentFieldsNoPagination(datasource_id, collectionDataName, queryObj, limit)
            .subscribe(res => {
              this.createChartInstance(visualizationOptionData, chartDiv, res.dataList)
            }, err => {});
    }

    createChartInstance(visualizationOptionData, chartDiv, chartData) {
    var _self = this;
    // Create map instance
    var chart = am4core.create(chartDiv, am4maps.MapChart);

    // Set map definition
    chart.geodata = am4geodata_worldLow;

    // Set map definition
    chart.geodata = am4geodata_worldLow;

    let restoreContinents = function(){
      chart.goHome();
    };

    // Zoom control
    chart.zoomControl = new am4maps.ZoomControl();

    let homeButton = new am4core.Button();
    homeButton.events.on("hit", restoreContinents);

    homeButton.icon = new am4core.Sprite();
    homeButton.padding(7, 5, 7, 5);
    homeButton.width = 30;
    homeButton.icon.path = "M16,8 L14,8 L14,16 L10,16 L10,10 L6,10 L6,16 L2,16 L2,8 L0,8 L8,0 L16,8 Z M16,8";
    homeButton.marginBottom = 10;
    homeButton.parent = chart.zoomControl;
    homeButton.insertBefore(chart.zoomControl.plusButton);

    // Shared
    let hoverColorHex = "#9a7bca";
    let hoverColor = am4core.color(hoverColorHex);
    let hideCountries = function() {
      // countryTemplate.hide();
      // labelContainer.hide();
    };


    // Set projection
    chart.projection = new am4maps.projections.Miller();

    // Create map polygon series
    var polygonSeries = chart.series.push(new am4maps.MapPolygonSeries());

    // Make map load polygon (like country names) data from GeoJSON
    polygonSeries.useGeodata = true;
    polygonSeries.data = chartData

    // polygonSeries.exclude = ["AQ"];

    // Configure series
    var polygonTemplate = polygonSeries.mapPolygons.template;
    polygonTemplate.tooltipText = "{name}";
    polygonTemplate.fill = am4core.color("#e0e0e0");
    polygonTemplate.cursorOverStyle = am4core.MouseCursorStyle.pointer
    polygonTemplate.events.on("hit", function(ev) {
      var data:any;
      data = ev.target.dataItem.dataContext;
      if (data.selected != undefined) {
      _self._sharedService.changeData(data)
      } else {}
    });


    // Create hover state and set alternative fill color
    var hs = polygonTemplate.states.create("hover");
    hs.properties.fill = chart.colors.getIndex(2);

    polygonTemplate.adapter.add("fill", function(fill, target) {
      let dataContext:any;
          dataContext = target.dataItem.dataContext
      if (dataContext && dataContext.selected) {
        // var pattern = new am4core.LinePattern();
        // pattern.width = 10;
        // pattern.height = 10;
        // pattern.stroke = am4core.color("#367B25");
        // pattern.strokeWidth = 1;
        // pattern.rotation = 45;
        return am4core.color("#d285e3");
      }
      return fill;
    });

    polygonTemplate.adapter.add("stroke", function(fill, target) {
      let dataContext:any;
          dataContext = target.dataItem.dataContext
      if (dataContext && dataContext.selected) {
        return am4core.color("#d285e3");
      }
      return fill;
    });
        return chart;
    }
}