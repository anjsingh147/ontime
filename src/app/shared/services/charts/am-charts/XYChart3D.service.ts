import {
    Component,
    OnInit,
    OnDestroy,
    ChangeDetectorRef,
    Injectable
} from '@angular/core';

import {
    ChartDataService
} from '../chart-data.service';

import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
am4core.useTheme(am4themes_animated);

@Injectable()
export class XYChart3DService implements OnInit {

    constructor(private _objectService: ChartDataService) {}

    ngOnInit() {}

//------------------------------------------------Methods------------------------------------------//


    makeChart(visualizationOptionData, chartDiv, data_source_id, collectionDataName, query , limit) {
        // this._objectService.getDataByCollectionNameWithDocumentFieldsNoPagination(data_source_id, collectionDataName, query)
        //     .subscribe(res => {
        //       this.createChartInstance(visualizationOptionData, chartDiv, res.dataList)
        //     }, err => {});
    }

    createChartInstance(visualizationOptionData, chartDiv, chartData) {
        let chart = am4core.create(chartDiv, am4charts.XYChart3D);

        // Add data
        chart.data = chartData;
        chart.responsive.enabled = true;
        // Create axes
        let categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = visualizationOptionData.selectedCategory;
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.minGridDistance = 10;

        let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.title.text = visualizationOptionData.valueAxisTitle;
        valueAxis.renderer.labels.template.adapter.add("text", function(text) {
          return text;
        });

        // Create series
        let series = chart.series.push(new am4charts.ColumnSeries3D());
        series.dataFields.valueY = visualizationOptionData.selectedseries1ValueY;
        series.dataFields.categoryX = visualizationOptionData.selectedCategory;
        series.name = visualizationOptionData.selectedseries1ValueY;
        series.clustered = false;
        series.columns.template.tooltipText = visualizationOptionData.series1tooltipText + ": [bold]{valueY}[/]";
        series.columns.template.fillOpacity = 0.9;
        series.columns.template.cursorOverStyle = am4core.MouseCursorStyle.pointer;
        series.columns.template.events.on("hit", function(ev) {
        }, this);

        let series2 = chart.series.push(new am4charts.ColumnSeries3D());
        series2.dataFields.valueY = visualizationOptionData.selectedseries2ValueY;
        series2.dataFields.categoryX = visualizationOptionData.selectedCategory;
        series2.name = visualizationOptionData.selectedseries2ValueY;
        series2.clustered = false;
        series2.columns.template.tooltipText = visualizationOptionData.series2tooltipText + ": [bold]{valueY}[/]";
        series2.columns.template.cursorOverStyle = am4core.MouseCursorStyle.pointer;
        series2.columns.template.events.on("hit", function(ev) {
        }, this);

        return chart;
    }
}