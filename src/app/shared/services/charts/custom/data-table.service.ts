import {
    Component,
    OnInit,
    OnDestroy,
    ChangeDetectorRef,
    Injectable
} from '@angular/core';

import {
    ChartDataService
} from '../chart-data.service';

import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
am4core.useTheme(am4themes_animated);

@Injectable()
export class DataTableService implements OnInit {

  loading: boolean = false;
    // page = new Page();
    columns = [];
    rows = []

    constructor(private _objectService: ChartDataService) {}

    ngOnInit() {}

//------------------------------------------------Methods------------------------------------------//


    makeChart(visualizationOptionData, chartDiv, datasource_id, collectionDataName, query , limit) {
        let queryObj = {}
        if (query != undefined) {
            queryObj = query
        }
        this._objectService.getDataByCollectionNameWithDocumentFieldsNoPagination(datasource_id, collectionDataName, queryObj, limit)
            .subscribe(res => {
              this.createChartInstance(visualizationOptionData, chartDiv, res.dataList)
            }, err => {});
    }

    createChartInstance(visualizationOptionData, chartDiv, chartData) {
        // let data = chartData[0];
        // let selectedFristLinkValue = '';
        // let selectedSecondLinkValue  = '';

        // let secondLinkText = '';
        // let fristLinkText = '';

        // if (data[visualizationOptionData.selectedFristLinkValue] != undefined) {
        //   selectedFristLinkValue  = data[visualizationOptionData.selectedFristLinkValue];
        // }

        // if (data[visualizationOptionData.selectedSecondLinkValue] != undefined) {
        //   selectedSecondLinkValue  = data[visualizationOptionData.selectedSecondLinkValue];
        // }

        // if (visualizationOptionData.fristLinkText  != undefined) {
        //   fristLinkText  = visualizationOptionData.fristLinkText;
        // }

        // if (visualizationOptionData.secondLinkText  != undefined) {
        //   secondLinkText  = visualizationOptionData.secondLinkText;
        // }

        let chart='';
        let el=document.getElementById(chartDiv);

         el.innerHTML=`<ngx-datatable class="material bg-white" 
   [loadingIndicator]="loading"
   [headerHeight]="50"
   [footerHeight]="50" 
   [scrollbarH]="false"
   [externalPaging]="true"
   [rowHeight]="50"
   [rows]="rows"
   [columns]="[{name: 'Name'}]"
   [count]="page.totalElements"
   [offset]="page.pageNumber"
   [limit]="page.size">
   </ngx-datatable>`;
        return chart;
    }
}

// transform: translateY(-50%);
// top: 50%;
// <p class="card-text">
//             `+
//             visualizationOptionData.cardText
//             +`
//             </p>