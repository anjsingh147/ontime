import {
    Component,
    OnInit,
    OnDestroy,
    ChangeDetectorRef,
    Injectable
} from '@angular/core';

import {
    ChartDataService
} from '../chart-data.service';

import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
am4core.useTheme(am4themes_animated);

@Injectable()
export class WidgetService implements OnInit {

    constructor(private _objectService: ChartDataService) {}

    ngOnInit() {}

//------------------------------------------------Methods------------------------------------------//


    makeChart(visualizationData, visualizationOptionData, chartDiv, datasource_id, collectionDataName, query , limit) {
        let queryObj = {}
        if (query != undefined) {
            queryObj = query
        }
        this._objectService.getDataByCollectionNameWithDocumentFieldsNoPagination(datasource_id, collectionDataName, queryObj, limit)
            .subscribe(res => {
              this.createChartInstance(visualizationData, visualizationOptionData, chartDiv, res.dataList)
            }, err => {});
    }

    createChartInstance(visualizationData, visualizationOptionData, chartDiv, chartData) {
      let cursor = 'auto';
      if (visualizationData != undefined) {
          if (visualizationData.visualization_leaf.visualization_id != undefined) {
              cursor = 'pointer';
          }
      }

        let data = chartData[0];
        let selectedFristLinkValue = '';
        let selectedSecondLinkValue  = '';

        let secondLinkText = '';
        let fristLinkText = '';

        if (data[visualizationOptionData.selectedFristLinkValue] != undefined) {
          selectedFristLinkValue  = data[visualizationOptionData.selectedFristLinkValue];
        }

        if (data[visualizationOptionData.selectedSecondLinkValue] != undefined) {
          selectedSecondLinkValue  = data[visualizationOptionData.selectedSecondLinkValue];
        }

        if (visualizationOptionData.fristLinkText  != undefined) {
          fristLinkText  = visualizationOptionData.fristLinkText;
        }

        if (visualizationOptionData.secondLinkText  != undefined) {
          secondLinkText  = visualizationOptionData.secondLinkText;
        }

        let chart='';
        let el=document.getElementById(chartDiv);

         el.innerHTML=`<div style="
         display: -webkit-box;
         display: -ms-flexbox;
         display: flex;
         -webkit-box-orient: vertical;
         -webkit-box-direction: normal;
         -ms-flex-direction: column;
         flex-direction: column;
         min-width: 0;
         word-wrap: break-word;
         background-color: #fff;
             padding: 0 15px;
         background-clip: border-box;
         position: relative;
         height: 100%;
  
         ">
         <div style="-webkit-box-flex: 1;
            -ms-flex: 1 1 auto;
            flex: 1 1 auto;
            color: #424242; cursor:`+ cursor + `;">

            <h2 style="letter-spacing: 2px; text-align:` 
            +
            visualizationOptionData.cardTitleTextAlign
            +`; color:` +

            visualizationOptionData.cardTitleColor
            +`; margin-bottom:` +
            
            visualizationOptionData.cardTitleMarginBottom 
            + `px; font-weight:` +
            
            visualizationOptionData.cardTitleFontWeight
            + `; font-size:` +
            
            visualizationOptionData.cardTitleFontSize 
            + `px; ">

            `+ data[visualizationOptionData.selectedCategory] +`

            </h2>

            <h3 style="margin-top:`
            +
            visualizationOptionData.cardSubtitleMarginTop 
            + `px; margin-bottom:` +

            visualizationOptionData.cardSubtitleMarginBottom 
            + `px; font-size:` +

            visualizationOptionData.cardSubtitleFontSize 
            + `px; color:` +

            visualizationOptionData.cardSubtitleColor 
            + `; font-weight:` +

            visualizationOptionData.cardSubtitleFontWeight 
            + `; text-align:` +
            visualizationOptionData.cardSubtitleTextAlign
            + `; ">`
            + visualizationOptionData.cardSubtitle + 
            
            `</h3>
            
            <div class="row" *ngIf="selectedFristLinkValue != ''" style="position: absolute;
    bottom: 40px;
    width: 100%;font-size:`+
            visualizationOptionData.linkFontSize 
            +
            `px; text-align:`

            +
visualizationOptionData.linkTextAlign
            +
            `;">
      <div class="col-xlg-6 col-lg-6 col-md-6 col-sm-6 col-xs-6">
      <a href="#" style="color:` +
visualizationOptionData.fristLinkColor
      +
      `;">`+

            selectedFristLinkValue
            +
            ` `+ fristLinkText  + `</a>
          </div>
          <div class="col-xlg-6 col-lg-6 col-md-6 col-sm-6 col-xs-6">
          <a  style="color:` +
visualizationOptionData.secondLinkColor
      +
      `;">`+
            selectedSecondLinkValue
            +
            ` `+ secondLinkText  + `</a>
          </div>
      </div>
      </div>
      </div>`;
        return chart;
    }
}

// transform: translateY(-50%);
// top: 50%;
// <p class="card-text">
//             `+
//             visualizationOptionData.cardText
//             +`
//             </p>