import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/Rx';
import 'rxjs/add/operator/map';

import { Config } from '../../../shared/services/config/general.config';
import { API_URL } from '../../../shared/services/config/env.config';
import { API_LOGIN_URL } from '../../../shared/services/config/env.config';


@Injectable()
export class HeaderTopService {
  apiRoute:string = 'users';
  apiRoleRoute:string = 'role-management';
  
  constructor(private _http: Http) {}

/*
* Calling api to get users data.
*/ 
    getUserDataByID(id){
        return this._http.get(API_URL + this.apiRoute +'/' + id, Config.apiAccessToken())
            .map(res => res.json())
            .catch(Config.handleError);
    }

    getRoleDataByOrgIdAndRoleName(org_id, role_name){
        return this._http.get(API_URL + this.apiRoleRoute + '/byOrgIdAndRoleName/' + org_id + '/' + role_name, Config.apiAccessToken())
            .map(res => res.json())
            .catch(Config.handleError);
    }
}
