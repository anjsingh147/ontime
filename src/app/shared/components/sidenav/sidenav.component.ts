import { Component, OnInit, Input } from '@angular/core';
import { Config } from '../../services/config/general.config';
import { USER_ROLES } from '../../services/config/env.config';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.template.html'
})
export class SidenavComponent {
  @Input('items') public menuItems: any[] = [];
  @Input('hasIconMenu') public hasIconTypeMenuItem: boolean;
  @Input('iconMenuTitle') public iconTypeMenuTitle: string;
  public usrRoleIndex:number = USER_ROLES.indexOf(Config.getUserRole())

  constructor() {}
  ngOnInit() {}

  // Only for demo purpose
  addMenuItem() {
    this.menuItems.push({
      name: 'ITEM',
      type: 'dropDown',
      tooltip: 'Item',
      icon: 'done',
      state: 'material',
      sub: [
        {name: 'SUBITEM', state: 'cards'},
        {name: 'SUBITEM', state: 'buttons'}
      ]
    });
  }

   public menuAccessCtrl(usrRole:string) {
     console.log(usrRole)
    return USER_ROLES.indexOf(usrRole);
  }
}