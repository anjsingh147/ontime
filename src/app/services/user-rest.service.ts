// import { Injectable } from '@angular/core';
// import { InterceptorService } from 'ng2-interceptors';

// @Injectable()
// export class UserRestService {

//   private baseUrl: string = 'http://184.168.123.236:5050/';

//   constructor(private http: InterceptorService) { 
//   }

//   register(registerData) {
//     return this.http.post(this.baseUrl +'employee-register', registerData )
//       .map(response => response.json());
//   }

//   login(loginData) {
//     return this.http.post(this.baseUrl + 'login', loginData)
//       .map(response => response.json());
//   }

//   updatePassword(updatePasswordData) {
//     return this.http.post(this.baseUrl + 'updatePassword', updatePasswordData)
//       .map(response => response.json());
//   }

//   updateImage(updateImageData) {
//     return this.http.post(this.baseUrl + 'updateImage', updateImageData)
//       .map(response => response.json());
//   }

//   home() {
//     return this.http.get(this.baseUrl + 'home')
//       .map(response => response.json());
//   }



// }


import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/Rx';
import 'rxjs/add/operator/map';

import { Config } from '../shared/services/config/general.config';
import { API_URL } from '../shared/services/config/env.config';
import { API_LOGIN_URL } from '../shared/services/config/env.config';


@Injectable()
export class UserRestService {
  apiRoute:string = 'users';
  apiRequestHeader:object = Config.apiAccessToken();
  private baseUrl: string = 'http://184.168.123.236:5050/';
  
  constructor(private http: Http) {}
  
/*................................Function to login user.....................................................................*/
  
  // login(user) {
  //   return this.http.post(API_LOGIN_URL + 'local', user).map(res => res.json()).catch(Config.handleError);
  // }

/*................................Function to register user accounts.....................................................................*/
  
  signUp(user){
    console.log(API_URL)
    return this.http.post(this.baseUrl + 'employee-register', user).map(res => res.json()).catch(Config.handleError);
  }

/*................................Function to store user's data in client local storage.....................................................................*/

  storeUserData(AuthToken) {
    Config.setLoggedInToken(AuthToken); // Set token in local storage
  }

// /*
// * Calling api to get users data.
// */ 
//  getUserDataByID(id){
//     return this.http.get(API_URL + this.apiRoute +'/' + id, Config.apiAccessToken())
//             .map(res => res.json())
//             .catch(Config.handleError);
//   }


  // constructor(private http: InterceptorService) { 
  // }

  register(registerData) {
    return this.http.post(this.baseUrl +'employee-register', registerData )
      .map(response => response.json());
  }

  login(loginData) {
    return this.http.post(this.baseUrl + 'employee-login', loginData)
      .map(response => response.json());
  }

  updatePassword(updatePasswordData) {
    return this.http.post(this.baseUrl + 'updatePassword', updatePasswordData)
      .map(response => response.json());
  }

  updateImage(updateImageData) {
    return this.http.post(this.baseUrl + 'updateImage', updateImageData)
      .map(response => response.json());
  }

  home() {
    return this.http.get(this.baseUrl + 'home')
      .map(response => response.json());
  }
}