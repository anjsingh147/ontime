

## **Installation**
* Install Node.js  if not already installed

    * Recommended Node version: >= v6.10, but latest version is always better 

* Install express, gulp, angular-cli and globally in your local development machine (not mandatory but recommended)

```shell
$ npm install express gulp angular-cli -g
```

* Clone the project repository

* Go to the cloned project's root directory and install all the dependencies required for Node.js


```shell
$ npm install
```

* After the root directory dependencies installation, go to client directory from the root directory and install all the dependencies required for Angular

```shell
$ cd client
```
```shell
$ npm install
```

## **Running Application in Production Environment**

* **To run the application in production environment, first the angular application should be built into static files so that those files could be served from the server.**


* **To generate dist files needed to run the application in production environment**
    * To generate deployment package of the admin app for production type, run the following commands: 

    
* After the successful build, run the application using the command below:
   
```shell
$ npm start
```

